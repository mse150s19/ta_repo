import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import cKDTree
from skimage.util import invert
from skimage import io
from skimage import color
from skimage.filters import threshold_otsu
from skimage.morphology import skeletonize
from skimage import data
from scipy import ndimage
from skimage.filters import sobel
import random

rad=1.5
bigNum=10000	#int for max iterations.
stepSze = 0.51

def main():

    # Make a binary image to work with.
    image = io.imread('mainspring1.png')

    thresh = threshold_otsu(image)
    binary = image > thresh
    img = invert(binary)

    #for the sample JPGs.
    #image = color.rgb2gray(io.imread('morePics/IMG_0142.JPG'))
    #imag = maskImage(image) 
    #filtrdImg = filterImage(imag) 
    #exit()

    # perform skeletonization
    skeleton = skeletonize(img)
    
    #print(skeleton.shape)
    rows, cols = np.where(skeleton)
    skelCoords = np.array([rows,cols]).T
    #print(skelCoords.shape[0])
    #print(skelCoords)

    # Set origin and end.
    #orig = np.array([787,248])
    orig = np.array([784,253])
    terminus = np.array([7,353])

    #plotter1(image,invert(skeleton),'skeleton')
    #exit()

    #get the main backbone of coil, removing extra pixels.
    skel_xy = trim_skel(skelCoords,orig,terminus)
    #print(skel_xy[:10])

    #skel = np.unique(skel_xy,axis=0)
    #print(skel.shape[0])

    doSciencePleaseThanks(skel_xy,orig)

    plotter(image,orig,skel_xy,skeleton)

def maskImage(image):

    #center point of coil.
    #cx = np.array([1645,1236])
    cx = np.array([1730,1350])
    nrows, ncols = image.shape
    row, col = np.ogrid[:nrows, :ncols]
    radView = 750**2

    img = np.copy(image)
    outer_disk_mask = ((row - cx[1])**2 + (col - cx[0])**2 > radView)

    img[outer_disk_mask] = 1
    #print(img.min(),img.max())

    #plotter1(image,img,'select region')

    #elevation_map = sobel(image)
    #plotter1(image,elevation_map,'elevation')

    return img

def filterImage(image):
 
    #otsu filtering.
    #thresh = threshold_otsu(image)
    #binary = image > thresh
    #print(binary.min(),binary.max())

    #binary hamhand.
    #mask1 = img > maskThresh
    #img[mask1] = 1

    #mask2 = img < maskThresh 
    #img[mask2] = 0

    #real routine.
    blurred_f = ndimage.gaussian_filter(image, 3)
    filter_blurred_f = ndimage.gaussian_filter(blurred_f, 1)
    alpha = 30
    sharpened = blurred_f + alpha * (blurred_f - filter_blurred_f)

    sx = ndimage.sobel(sharpened, axis=0, mode='constant')
    sy = ndimage.sobel(sharpened, axis=1, mode='constant')
    sob = np.hypot(sx, sy)

    #binary hamhand.
    #maskThresh=0.81
    #mask1 = sharpened > maskThresh
    #sharpened[mask1] = 1

    #mask2 = sharpened < maskThresh 
    #sharpened[mask2] = 0

    plotter1(image,invert(sob),'select region')

    return sharpened


def trim_skel(skelCoords,orig,terminus):

    #Put the skeleton coordinates on a tree.
    skelTree = cKDTree(skelCoords)

    #Get the orig/term indices, and their nearest neighbors in the array.
    d0,indxOrig = skelTree.query(orig,k=2)
    dT,indxTerm = skelTree.query(terminus,k=2)
    #And their coords.
    orig_xy = np.array(skelCoords[indxOrig])
    term_xy = np.array(skelCoords[indxTerm])
    #print(orig_xy,term_xy)

    #iteratively trim the tree.
    done = False
    while( not done):

        #Create tree, again, and with every iteration.
        skelTree = cKDTree(skelCoords)
        lenOld = len(skelCoords)
        #print("before trim: ", lenOld)

        #find the leaves/juncs/ends of the tree.
        loners,juncs,ends = find_leafs(skelTree,skelCoords)
        #print("loners: ", loners)
        #print("juncs: ", juncs)
        #print("ends: ", ends)
 
        #Make an index list of the unique ends.
        uniqEnds = np.unique(ends.flatten()).tolist()
        #print(uniqEnds)
        #endCoords = np.array(skelCoords[uniqEnds])
        #print(endCoords)

	#take away the origin and terminus (and their nearest neighbors) from
        #index list.  
        remList1=np.delete(uniqEnds,indxOrig).tolist()
        remList=np.delete(remList1,indxTerm).tolist()
        #print("remove List: ",remList)
        #print("remove Coords: ",skelCoords[remList])
        #print("LENGTH remove List: ",len(remList))

        #add the loners - extra pixels - to the list for removal.
        remList.extend(loners.flatten().tolist())
        #print(remList)

        #endCoords consists of just those points to remove.
        #endCoords = np.array(skelCoords[remList])

        #delete all ends remaining from the coordinate list.
        sk2=np.delete(skelCoords,remList,0)
        #print(len(sk2))

        #add the origin and terminus back.
        sk2 = np.vstack((sk2,orig_xy,term_xy))
        #print(len(sk2))
    
        #plotter(image,orig,sk2,skeleton)

        skelCoords=np.copy(sk2)
        #print("after trim2: ",len(skelCoords))

        #when no trimming is left to be performed, exit.
        #lenUntrimmed = len(sk2)
        #if lenUntrimmed == lenOld:

        if len(remList) == 0:   
            done = True
 
    #make unique - added the orig/terminus a bunch, I think.
    skel_coords = np.unique(skelCoords,axis=0)

    #order the coordinates from origin out.
    skelcoords = order_coords(skel_coords,orig,terminus)

    #print(skel_coords)

    #and return skelcoords
    #return skel_coords
    return skelcoords


def find_leafs(T,skelCoords):

    #First, find all ends where there is only one nearest neighbor, or fewer.
    endBranches = []
    isolatd = []
    juncts = []
    numCoords = skelCoords.shape[0]
    for i in range(0,numCoords):
        target = skelCoords[i]
        ii = T.query_ball_point(target,r=rad)

        if len(ii) == 1: 
            #print("LONER: ", i, skelCoords[i],ii)
            isolatd.append(ii)
        elif len(ii) == 2: 
            #print("TAIL: ", i, skelCoords[i],ii)
            endBranches.append(ii)
        elif len(ii) == 4: 
            juncts.append(ii)
            #print("JUNC: ", i, skelCoords[i],ii)
    ends = np.array(endBranches)
    loners = np.array(isolatd)
    juncs = np.array(juncts)
    return (loners,juncs,ends)

def order_coords(skelCoords,origXY,termXY):

    #predictor-corrector search for nearest neighbor, non-identity.

    #Create a tree for query.
    skeletonTree = cKDTree(skelCoords) 

    #Get the origin and nearest neighbor indices.
    dOrig,indxOrig = skeletonTree.query(origXY,k=2)
    orderd=np.array(indxOrig)

    #index of the terminus.
    dTerm,indxTerm = skeletonTree.query(termXY,k=1)
    #print("Terminus: ",indxTerm)
    
    numAttmpts = 0
    numSuccess = 0
    tossl = np.zeros(2, dtype=float)

    done = False
    while( not done):
         numAttmpts += 1
         v1 = skelCoords[orderd[-2]] 
         v2 = skelCoords[orderd[-1]] 
         predctVec = unitVec(v2-v1)
         v3 = v2+predctVec+tossl
         tossl[0] = 0.
         tossl[1] = 0.
         #print("attempts: ",numAttmpts,"\tSuccess: ",numSuccess)
         
         dOrig,indxOrig = skeletonTree.query(v3,k=2)

         if indxOrig[0] != orderd[-1]:        #if not same as prev.
             orderd = np.append(orderd, indxOrig[0])
             numSuccess += 1
             #print("iter: ",i, dOrig,"\t",indxOrig,"\t",orderd)
         else:
             tossl += (stepSze*arbStep())

         if (indxTerm in orderd) or (numAttmpts>bigNum):   
             done = True
             if(numAttmpts>bigNum):
                 print('\n')
                 print('Iterated to death, could not trace curve!')
                 print('Try a larger step forward in the tracing algorithm for the traveling salesman (stepSze)...') 
                 print('\n')
                 print('...now exiting, bye...')
                 print('\n')
                 exit()

    numCoords = skelCoords.shape[0]
    print("Found ", numSuccess, "points of ", numCoords,  " total.", numSuccess/numCoords) 
    print("Algorithm efficiency (irrel.) ", numSuccess/numAttmpts)
    

    #exit()
    #numCoords = skelCoords.shape[0]

    #OLDER, to return.
    #target = origXY
#
    #done = False
    #while( not done):
        ##d_,indX = skeletonTree.query(target,k=3)
#
        ##find those points around the target.
        #indX = skeletonTree.query_ball_point(target,r=rad)
        ##print(indX)
        ##add them if they are absent.
        #for i in indX:
            #if i not in orderd:
                #orderd.append(i) 
        #target = skelCoords[orderd[-1]]
        ##print(orderd)
#
        #if indxTerm in orderd:   
            #done = True
 #
    skel_XY = np.array(skelCoords[orderd])

    #print(skel_XY[:10])

    return skel_XY

def unitVec(v):
    #norm = np.linalg.norm(v)
    norm = np.sqrt((v**2).sum())
    if norm == 0: 
       return v
    return v/norm

def plotter1(image,skeleton,strngTitle):

    # display results
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4),sharex=True,sharey=True)

    #ax = axes.ravel()
    #ax = axes

    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].axis('off')
    ax[0].set_title('original', fontsize=20)

    ax[1].imshow(skeleton, cmap=plt.cm.gray)
    #ax[1].imshow(invert(skeleton), cmap=plt.cm.gray)
    #ax[1].plot(sk3[:,1],sk3[:,0],'.',c='r',markersize=2.5)
    #ax[1].plot(endCoords[:,1],endCoords[:,0],'.',c='r',markersize=2.5)
    ax[1].axis('off')
    ax[1].set_title(strngTitle, fontsize=20)

    fig.tight_layout()
    plt.show()
    #plt.savefig("coilSkeleton.png")

def arbStep():

    theta = 2*math.pi*random.random()
    vec = np.array([math.cos(theta),math.sin(theta)],float)
    return vec

def plotter(image,endCoords,sk3,skeleton):

    # display results
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4),sharex=True,sharey=True)

    #ax = axes.ravel()
    #ax = axes

    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].axis('off')
    ax[0].set_title('original', fontsize=20)

    ax[1].imshow(invert(skeleton), cmap=plt.cm.gray)
    ax[1].plot(sk3[:,1],sk3[:,0],'.',c='r',markersize=2.5)
    #ax[1].plot(endCoords[:,1],endCoords[:,0],'.',c='r',markersize=2.5)
    ax[1].axis('off')
    ax[1].set_title('skeleton, trimmed', fontsize=20)

    fig.tight_layout()
    #plt.savefig("coilSkeletonTrimmed.png")
    plt.show()


def doSciencePleaseThanks(skel,origXY):

    a=1.5
    b=0.22

    #get coordinates as a function of angle.
    ii = 0
    windingNum = 0
    vecSav = (-1)*np.ones(2, dtype=int)
    rads = []
    nmb = []
    thetas = []
    logspiral = []

    for coord in skel:
        v = coord - origXY
        #incrLngth = np.sqrt((v**2).sum())
        rad = np.sqrt((v**2).sum())
        theta = np.arctan2(v[0],v[1])
        #print("vec: ",v[1],v[0])
        if ( (v[0] < 0) and (vecSav[0] >= 0) ): #and (vecSav[1] >= 0) ):
            windingNum += 1
            #print("wound")
        theta += 2.*windingNum*np.pi
        #print(rad,theta)
        ii += 1
        rads.append(rad)
        thetas.append(theta)
        nmb.append(ii)
        vecSav = np.copy(v)
        #print("vecSav: ",vecSav[1],vecSav[0])
        logspiral.append(a*np.exp(b*theta))
      #  print(theta,rad)
        

    #print(coord[0],coord[1])
    #plt.plot(thetas,rads,'.')
   # plt.plot(nmb,logspiral,'.',c='orange')
    #plt.title('r vs ordered pixel')
    #plt.xlim([10,200])
    #plt.xlabel('black pixel #, ordered from origin')
    #plt.ylabel('radial dist from origin, in pixels')
    #plt.savefig('radialTrajTest.png')

    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4),sharey=True)

    ax[0].plot(nmb,rads,'.')
    ax[0].set_xlabel('black pixel #, ordered from origin')
    ax[0].set_ylabel('radial distance from origin, in pixels', fontsize=14)
    ax[0].set_ylim([0,830])

    ax[1].plot(thetas,rads,'.',c='r')
    ax[1].plot(thetas,logspiral,'-',c='dodgerblue')
    ax[1].set_xlabel('angle theta')


    ax[1].xaxis.set_ticks(np.arange(0,10*np.pi,np.pi))
    _=plt.xticks(rotation=75)

    fig.tight_layout()
    #plt.savefig("coilSkeletonTrimmed.png")
    plt.show()


if __name__ == '__main__':
    main()

